# -*- coding: utf-8 -*-
from webisad import app, login_manager
from webisad.lidar.lidarviews import lidarpage

app.register_blueprint(lidarpage,url_prefix='/lidar')

login_manager.setup_app(app)
app.run(host="0.0.0.0",debug=True)
