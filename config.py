import os
basedir = os.path.abspath(os.path.dirname(__file__))

#SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://webisad:123123@localhost:5432/webisad'
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
UPLOAD_FOLDER = 'webisad/UPLOADS'

METEO_FOLDER = 'webisad/NCMETEO'
DHT11_DATA = 'DHT11'

ALLOWED_EXTENSIONS= set(['txt','doc','pdf'])
ALLOWED_EXT_METEO = set(['nc4','nc'])

MAX_CONTENT_LENGTH=16*1024*1024
SECRET_KEY='123123123'


