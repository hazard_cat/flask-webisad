# -*- coding: utf-8 -*-
from webisad import app, db
from flask import render_template, url_for, request, flash, redirect, send_from_directory, g
from flask.ext.login import current_user, login_required, login_user, logout_user, fresh_login_required
from werkzeug import secure_filename
from webisad.models import NewsModel, UserModel
from hashlib import md5
import datetime
from flask.ext.babel import gettext
from datetime import datetime
from webisad.forms import LoginForm, AddNewsForm
import numpy as np

import os
from datetime import datetime


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.',1)[1] in app.config['ALLOWED_EXTENSIONS']
        

@app.template_filter('date')
def _jinja2_filter_datetime(date):
    return date.strftime('%d/%m/%Y')


from webisad import  login_manager

@app.before_request
def before_request():
    g.user = current_user


@login_manager.user_loader
def load_user(id):
    user=UserModel.query.get(id)
    return user


@app.route('/')
def index():
#    return current_user.name
    print current_user
    return render_template('index.html', recent_news=recent_news())
    
    
@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out.")
    return redirect(url_for("index"))


@app.route("/login", methods=["GET", "POST"])
def login():
    if g.user is not None and g.user.is_authenticated():
        return redirect(url_for('index'))

    form = LoginForm()

    if form.validate_on_submit():
        username = form.login.data
        user=UserModel.query.filter(UserModel.login==username).all() 
        print user
        if len(user)!=0:
            remember = form.remember_me.data
            passwd = md5(form.passwd.data).hexdigest()
            if (passwd==user[0].passwd) and (login_user(user[0], remember=remember)):
                flash("Logged in!")
                return redirect(request.args.get("next") or url_for("index"))
            else:
                flash("Sorry, but you could not log in.")
        else:
           flash(u"Invalid username.")
    return render_template("login.html", form=form)

    
    
    
@app.route('/about')
def about():
    return render_template("about.html")
    
@app.route('/uploads/<filename>')
def uploaded(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)


@app.route('/upload', methods=['GET','POST'])
@fresh_login_required
def upload_file():
    if request.method == 'POST':
        file=request.files['datafile']
        if file and allowed_file(file.filename):
            filename=secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            #redirect(url_for('uploaded', filename=filename))
            flash("File %s successfully loaded!"%filename)
            return redirect(url_for('upload_file'))
        else:
            flash('Can''t upload file %s'%file.filename)
            return redirect(url_for('upload_file'))
    return render_template('upload.html')


@app.route('/selectdata', methods=['GET','POST'])
@login_required
def select_data():
    if request.method == 'POST':
        parameter = request.form['param']
        startt = request.form['startt']
        stopt  = request.form['stopt']
        flash("Your request has been processed!")
        return redirect(url_for('show_meteo', param=parameter, t0=startt, t1=stopt))
    return render_template('select.html')
    
import matplotlib.dates as mdates
from matplotlib.figure import Figure                         
from matplotlib.backends.backend_agg import FigureCanvasAgg
import cStringIO
    
@app.route('/show_meteo', methods=['POST','GET'])
def show_meteo():    
    
    lines = None
    fname=os.path.join(app.config['DHT11_DATA'],'meteo.txt')
    with open(fname,'rt') as f:
	lines = f.readlines()

    time = []
    T = []
    H = []
    for line in lines:
	parts = line.strip().split(',')
	time.append(datetime.strptime(parts[0],'%Y%m%d%H%M%S'))
	T.append(float(parts[1]))
	H.append(float(parts[2]))
    #time = np.array(time)
    #T=np.array(T)
    #H=np.array(H)
    
    fig = Figure(figsize=[10,7])                                  
    ax1 = fig.add_axes([.12,.12,.8,.8])                             
    ax1.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    ax1.xaxis.set_major_locator(mdates.DayLocator())
    
    ax2= ax1.twinx()
    ax1.step(time, T,'b.--')
    ax1.set_ylabel('T, *C')
    ax2.step(time, H,'go--')
    ax2.set_ylabel('RH, %')
    
    for tl in ax1.get_yticklabels():
        tl.set_color('b')
        
    for t2 in ax2.get_yticklabels():
        t2.set_color('g')
    ax1.grid(True)
    fig.autofmt_xdate()
    canvas = FigureCanvasAgg(fig)
        
    buf = cStringIO.StringIO()
    canvas.print_png(buf)
    
    data = buf.getvalue().encode("base64").replace("\n", "")
    return render_template('image.html', data=data)


@app.route('/add_news', methods=['GET','POST'])
@login_required
def add_news():
    form= AddNewsForm()
    if form.validate_on_submit():
        title = form.title.data
        category = form.category.data
        text = form.text.data
	
        try:
            news = NewsModel(title=title, category=category, text=text, date=datetime.utcnow(), author=current_user)
            db.session.add(news)
            db.session.commit()
        except Exception, e:
            print e
            flash("Error!!!")
        flash("News were successfully added.")
        
        return redirect(url_for("add_news"))
    return render_template("addnews.html", form=form)


@app.route('/expeditions/<year>')
def expeditions(year):
    return year
    
def recent_news():
    news = NewsModel.query.order_by('date').all()
    news.reverse()
    return news
    
    
    