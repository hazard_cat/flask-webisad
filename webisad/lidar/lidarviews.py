#-*- coding: utf-8 -*-

from flask import  render_template
from jinja2 import TemplateNotFound
from webisad.lidar import lidarpage

@lidarpage.route('/', defaults={'page':'index'})
@lidarpage.route('/<page>')
def show(page):
    try:
	tmpl = 'pages/%s.html'%page
	print tmpl
	return render_template(tmpl)
    except TemplateNotFound,e:
	print e
	abort(424)

