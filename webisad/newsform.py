from flask.ext.wtf import Form, TextField, TextAreaField
from flask.ext.wtf import Required



class NewsForm(Form):
    title=TextField('Title', validators = [Required()])
    category=TextField('Category', validators=[Required()])
    text = TextAreaField()
    
    