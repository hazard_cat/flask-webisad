# -*- coding: utf-8 -*-
"""
Created on Thu Jun 13 15:38:42 2013

@author: Администратор
"""

from webisad import db

from datetime import datetime
from flask.ext.login import UserMixin,  AnonymousUserMixin


ROLE_USER = 0
ROLE_MODER = 1
ROLE_ADMIN = 2


class NewsModel(db.Model):
    __tablename__ = 'news'
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime())
    title = db.Column(db.String(80))
    category = db.Column(db.String(80))
    text = db.Column(db.String(255))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    
    def __repr__(self):
        return '<News %r>' % (self.title)    
        
        
class UserModel(db.Model):
    __tablename__='users'
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(40), unique=True)
    passwd= db.Column(db.String(40))
    role = db.Column(db.Integer(), default=ROLE_USER)#0-user(view only), 1-moder (add, view, delete own), 2-admin (all)
    email = db.Column(db.String(40), unique=True)
    organization = db.Column(db.String(255))
    news = db.relationship('NewsModel',backref = 'author', lazy = 'dynamic')
    

    def is_authenticated(self):
        return True
    

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User %r>' % (self.login)


class Anonymous(AnonymousUserMixin):
    login='Anonymous'
