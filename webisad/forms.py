from flask.ext.wtf import Form, TextField, BooleanField, PasswordField, TextAreaField
from flask.ext.wtf import Required


class LoginForm(Form):
    login = TextField('username',  validators = [Required()])
    passwd= PasswordField('password', validators = [Required()])
    remember_me = BooleanField('remember_me', default = False)

class AddNewsForm(Form):
    title=TextField('Title', validators = [Required()])
    category=TextField('Category', validators=[Required()])
    text = TextAreaField('text', validators=[Required()])
